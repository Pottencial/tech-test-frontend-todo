## INSTRUÇÕES PARA O TESTE TÉCNICO

- Crie um fork deste projeto (https://gitlab.com/Pottencial/tech-test-frontend-todo/-/forks/new). É preciso estar logado na sua conta Gitlab;
- Adicione @Pottencial (Pottencial Seguradora) como membro do seu fork. Você pode fazer isto em  https://gitlab.com/`your-user`/tech-test-frontend-todo/settings/members;
 - Quando você começar, faça um commit vazio com a mensagem "Iniciando o teste de tecnologia" e quando terminar, faça o commit com uma mensagem "Finalizado o teste de tecnologia";
 - Commit após cada ciclo de refatoração pelo menos;
 - Não use branches;
 - Você deve prover evidências suficientes de que sua solução está completa indicando, no mínimo, que ela funciona;
 - Não há restrição quanto ao uso de bibliotecas de apoio;

## O TESTE
- Construir uma aplicação React (ES ou TypeScript);
- A aplicação deve permitir:
  1) Criação/edição/exclusão de tarefas contendo descrição, título e data/hora de inserção;
  2) Listar tarefas cadastradas;
  3) Alternar estado da tarefa entre concluída e pendente;
  
- A aplicação não precisa ter mecanismos de autenticação/autorização;
- A aplicação não precisa implementar os mecanismos de persistência em um banco de dados, eles podem ser persistidos "em memória".

## BÔNUS
- Permitir ordenação de tarefas pelo título e data/hora de inserção;
- Permitir filtrar a listagem de tarefas pesquisando por parte do conteúdo;

## PONTOS QUE SERÃO AVALIADOS
- Arquitetura da aplicação - embora não existam muitos requisitos de negócio, iremos avaliar como o projeto foi estruturada, bem como camadas e suas responsabilidades;
- Boas práticas e princípios como SOLID, DRY, KISS;
- Testes unitários;
- Responsividade.
